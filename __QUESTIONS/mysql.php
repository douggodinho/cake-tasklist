<?php

// no arquivo refatorado
class Users
{

	//... coisas antes

	public function getList($orderBy = 'name ASC')
	{

		$rows = DB::query('SELECT name FROM users ORDER BY ' . $orderBy, true);
		return $results;
	}

	//... coisas depois
}


// no local apropriado...
class DB {

	protected static $connection;

	public static init () {

		// procedimento para bootstrap da classe de PDO
		// ...
		// ...
		// ...
	}

	public static query($sql, $asArray = false){

		// wraper para método que consulta da classe de PDO
		// ...
		// ...
		// ...
	}
}
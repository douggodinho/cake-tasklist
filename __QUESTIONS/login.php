<?php

// no arquivo refatorado
if (Auth::isLoggedIn()) {

	header("Location: http://www.google.com");
	exit();
}


// no local apropriado...
class Auth {

	protected static $user;

	public static init () {

		// procedimento para bootstrap da classe de autenticação
		// ...
		// ...
		// ...
	}

	public static isLoggedIn(){

		// checagem de maneira realmente segura do usuário logado
		// ...
		// ...
		// ...
	}
}
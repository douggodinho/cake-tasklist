var app = angular.module('tasklist',['dndLists']);

app.controller('basic', function($scope, $http) {

    // inicia oarray de tarefas
    $scope.tasks = [];

    // default JSON request
    $http.defaults.headers.common['Accept'] = 'application/json';

    // obtem as tarefas
    $http.get('/tasks', 'JSON').success(function(data) {
        $scope.tasks = data.tasks;
    });

    // delete action
    $scope.delete = function (task) {
        if(confirm('Deseja mesmo deletar esta tarefa?')){
            $http.delete('/tasks/'+ task.id).success(function (data) {
                $scope.tasks.splice($scope.tasks.indexOf(task), 1);
            })
        }
    }

    // update action
    $scope.update = function (task) {
        $http.put('/tasks/'+ task.id, task).success(function (data) {

        })
    }

    // insert action
    $scope.insert = function (task) {
        if (!task.description.length)
            return;
        $http.post('/tasks', task).success(function (data) {
            task.description = '';
            $scope.tasks.unshift(data.task);
        })
    }

    // insert action
    $scope.sort = function (tasks) {

        var sort = [];
        tasks.forEach(function(task){
            sort.push(task.id);
        });

        console.log(sort);
        $http.post('/tasks/sort', {sort: sort}).success(function (data) {
            console.log(data);
            //task.description = '';
            //$scope.tasks.push(data.task);
        });
    }

});
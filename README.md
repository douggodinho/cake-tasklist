# CakePHP Tasklist

### Simples lista de tarefas implementada com CakePHP 3.1

- [Descrição](#markdown-header-descricao)
- [Utilização](#markdown-header-utilizacao)
- [TODO](#markdown-header-todo)


## Descrição

Simples lista de tarefas implementada com CakePHP 3.x. É necessária a instalação prévia do PHP (5.5+) e Composer.

No diretório __QUESTIONS estão contidas as demais tarefas solicitadas. São lógicas simples e auto explicativas.

## Utilização

Após o clone do repositório, para colocá-lo em funcionamento é necessário rodar os seguintes comandos:

    composer update
    # setup do mysql em config/app.php linhas: 210~220
    bin/cake migrations migrate
    bin/cake server

Após isso o app estará disponível em http://localhost:8765 (a porta pode variar em alguns casos)


## TODO

- Melhorar a documentação
- Melhorar a UX (principalmente o drag n drop)
- ...
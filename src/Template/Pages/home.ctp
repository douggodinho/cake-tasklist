<?php

$this->layout = 'basic';
?>

<div class="container">

    <br>

    <div class="input-group" ng-init="task={}">
        <span class="input-group-addon">+</span>
        <input class="form-control" type="text" ng-model="task.description" ng-keypress="($event.which == 13)?insert(task):0">
        <span class="input-group-btn">
          <button class="btn btn-default" type="button" ng-click="insert(task)">inserir</button>
        </span>
    </div>

    <br>

    <div class="list" dnd-list="tasks">

        <div class="input-group item" ng-repeat="task in tasks"
             dnd-draggable="task"
             dnd-dragend="sort(tasks)"
             dnd-moved="tasks.splice($index, 1)">
            <span class="input-group-addon">{{ task.id }}</span>
            <input class="form-control" type="text" ng-model="task.description" ng-change="update(task)">
            <span class="input-group-btn">
              <button class="btn btn-default" type="button" ng-click="delete(task)">delete</button>
            </span>
        </div>

    </div>

</div>
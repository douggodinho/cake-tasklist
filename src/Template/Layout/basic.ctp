<!DOCTYPE html>
<html>
<head>

    <?= $this->Html->charset() ?>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CakePHP Tasklist</title>

    <?= $this->Html->css([
        'http://bootswatch.com/slate/bootstrap.min.css',
        'app.css',
    ]) ?>
</head>
<body ng-app="tasklist" ng-controller="basic">

        <div class="header text-center">
            <h1>CakePHP Tasklist <br> <small>Simples lista de tarefas implementada com CakePHP 3.1</small></h1>
        </div>

        <?= $this->fetch('content') ?>

        <?= $this->Html->script([
            'https://code.jquery.com/jquery-2.1.4.min.js',
            'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js',
            'https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js',
            'http://marceljuenemann.github.io/angular-drag-and-drop-lists/angular-drag-and-drop-lists.min.js',
            'app.js',
        ]) ?>

</body>
</html>
